/*
 *  © 2011-2016 Vladimir Stadnik, <mailto:x-doggy at ya.ru>
 *  Main script of my tic-tac-toe web game
 */


var
    field_size = 3,                                   // Размерность доски
    cell = new Array(field_size),                     // Массив клеток доски
    current = "",                                     // Строковый тип игрока
    count = 0,                                        // Порядковый № игры
    player = false,                                   // Текущий игрок: false=о, true=x
    gaming = true,                                    // Состояние игры
    index = null,                                     // Индекс текущей клетки
    tds = document.getElementById("xo-game-table")
        .getElementsByTagName("td"),                  // Псевдомассив клеток


    // Табло "кто как ходит"
    msg = {
        elem: document.getElementById("xo-message"),
        add: function (text) { this.elem.value += text; },
        get: function () { return this.elem.value; },
        reset: function () { console.log(this.elem); this.elem.value = ""; }
    },
    // Табло "кто куда ходил"
    log = {
        elem: document.getElementById("xo-logs"),
        add: function (text) { this.elem.innerHTML += text; }
    },
    // Табло "кто как выиграл"
    stat = {
        elem: document.getElementById("xo-stats"),
        add: function (text) { this.elem.innerHTML += text; }
    },
    // Табло очков
    score = {
        x: {
            elem: document.getElementById("xo-x"),    // Крестика
            add: function (text) { this.elem.innerHTML = text; }
        },
        o: {
            elem: document.getElementById("xo-o"),    // Нолика
            add: function (text) { this.elem.innerHTML = text; }
        },
        n: {
            elem: document.getElementById("xo-n"),    // Ничьи
            add: function (text) { this.elem.innerHTML = text; }
        }
    }
// Кнопки хранения истории партий
info = {
    wins: [0, 0, 0],     // Победы: крестика, нолика, ничья
    place: [],           // Расположение элементов
    struct: []           // Порядок ходов игроков
};


/*
 * Функция выводит двумерный массив в две строки
 */
function _alertArr(arr) {
    if (typeof arr != "object" || !(arr instanceof Array)) return;
    var res = "";
    for (var i = 0, len = arr.length; i < len; i++) {
        for (var j = 0; j < len; j++) {
            switch (arr[i][j]) {
                case 1: res += "×  "; break;
                case 2: res += "o  "; break;
                default: res += "–  "; break;
            }
        }
        res += "\n";
    }
    _alert(res);
}

/*
 * Функция заменяет стандартный alert.
 */
function _alert(msg) {
    alert(msg);
}

/*
 * Функция инциализирует игорвую доску "новой игры".
 * Также происходит установка значений "0" массива клеток.
 */
function initTable() {
    for (var i = 0; i < field_size; i++) {
        cell[i] = new Array(field_size);
        for (var j = 0; j < field_size; j++)
            cell[i][j] = 0;
    }
}

/*
 * Функция проверяет состояние игры на выигрыш.
 * Возвращает True, если есть признак выигрыша:
 * по горизонтали, по вертикали и на диагонали.
 */
function isGame(mode) {
    for (var i = 0; i < field_size; i++)
        if (cell[i][0] === mode && cell[i][1] === mode && cell[i][2] === mode ||
            cell[0][i] === mode && cell[1][i] === mode && cell[2][i] === mode ||
            cell[0][0] === mode && cell[1][1] === mode && cell[2][2] === mode ||
            cell[0][2] === mode && cell[1][1] === mode && cell[2][0] === mode
        )
            return true;

    return false;
}

/*
 * Функция проверяет состояние игры на ничью.
 */
function isDraw() {
    for (var i = 0; i < field_size; i++)
        for (var j = 0; j < field_size; j++)
            if (cell[i][j] === 0)
                return false;
    return true;
}

/*
 * Функция обрабатывает 1-щелчок по клетке на игровом поле.
 * Вычисляется индекс клетки на [0;8] по 2-мерным координатам.
 * Устанавливается атрибут "ненажимаемый" (data-disable)
 * для невозможности повторного нажатия и изменения состояния
 * клетки.
 * В клетке рисуется соответствующий значок в зависимости от
 * текущего игрока.
 * В табло выдаётся сообщение о том, кто куда ходит.
 * Проверяется последняя ли это заполненная клетка и признак
 * выигрыша игрока, либо ничьи.
 * После совершения хода сменяется игрок на другого.
 */
function setValue(pos_x, pos_y) {
    if (!gaming) return;

    index = pos_x * field_size + pos_y; // порядковый индекс клетки

    if (tds[index]["data-disable"]) return;
    tds[index]["data-disable"] = true;

    var temp;
    if (!player) {
        temp = 1;
        current = "крестик";
        tds[index].innerHTML = "<span style=\"color: #c06;\">×</span>";
    } else {
        temp = 2;
        current = "нолик";
        tds[index].innerHTML = "<span style=\"color: #06c;\">o</span>";
    }

    cell[pos_x][pos_y] = temp;
    msg.add("Ходит " + current + " в (" + pos_x + "; " + pos_y + ")\n");

    if (isGame(temp) || isDraw()) { // есть признак окончания игры...
        gaming = false;
        count++;

        if (!isGame(temp) && isDraw()) { // ...когда ничья...
            _alert("Ничья!");
            info.wins[2]++;
        } else {                           // ...или когда победа игрока
            _alert("Победил " + current);
            info.wins[--temp]++;
        }

        // Записываем результаты в поля
        score.x.add(info.wins[0]);
        score.o.add(info.wins[1]);
        score.n.add(info.wins[2]);

        info.place.push(cell);
        info.struct.push(msg.get());

        stat.add("<li><button onclick=\"_alert(info.struct[" + (count - 1) + "]);\">Порядок ходов</button></li>");
        log.add("<li><button onclick=\"_alertArr(info.place[ " + (count - 1) + "]);\">Расположение элементов</button></li>");

        restartGame();

    }

    player = !player; // меняем игрока
}

/*
 * Функция сбрасывает состояние текущей партии
 * и готовит "новую игру".
 * Очищается игровое поле.
 * Сбрасывается состояние "ненажимаемый" у клеток.
 * Табло "кто куда ходит" очищается.
 * Производится инциализация игрового поля.
 */
function restartGame() {
    gaming = true;
    cell = new Array(field_size);

    for (var i = 0; i < field_size * field_size; i++) {
        tds[i].innerHTML = "&nbsp;";
        tds[i]["data-disable"] = false;
    }

    msg.reset();
    initTable();
}

/*
 * Последующие строки навешивают обработчики событий
 * на кнопки, а также загрузку игры при загрузке документа
 * и залёную кнопочку сброса игры.
*/
tds[0].onclick = function () { setValue(0, 0); }
tds[1].onclick = function () { setValue(0, 1); }
tds[2].onclick = function () { setValue(0, 2); }
tds[3].onclick = function () { setValue(1, 0); }
tds[4].onclick = function () { setValue(1, 1); }
tds[5].onclick = function () { setValue(1, 2); }
tds[6].onclick = function () { setValue(2, 0); }
tds[7].onclick = function () { setValue(2, 1); }
tds[8].onclick = function () { setValue(2, 2); }

document.getElementById("xo-nice-ver-button").onclick = restartGame;
window.onload = initTable;
